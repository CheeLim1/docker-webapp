#consider to use alpine node docker image
FROM node:latest 
# we specify the image name that we need to our project
WORKDIR /usr/src/app
# setting the path of the working directory
COPY package*.json ./
# copying both package.json & package-lock.json
RUN npm install
# installing all the dependencies
COPY . .
# copying all the rest of the files inside our project
EXPOSE 8080
# exposing the port which our application runs on
CMD ["node", "app.js" ]
# here, you can enter the command which we use to run our application 