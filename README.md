# Build docker image
docker build . -t hello-world
[Note: hello-world is the name of our docker image ]
Make sure your path is set to the project folder in the terminal.


docker run -p 8080:3000 --name c1 hello-world
[Note: port 8080 belongs to our local machine and our app runs on port 3000. c1 is the name of the container and again hello-world is the docker image ]

# Verify container running
docker ps
docker ps <container name>